{
  self,
  system,
  ...
}: final: prev: {
  fetchurl = self.inputs.nixpkgs.legacyPackages."${system}".fetchurl;
}
