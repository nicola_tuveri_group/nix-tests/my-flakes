{
  lib,
  stdenv,
  #openssl,
  openssl_3_2,
  oqs-provider,
}: let
  openssl = openssl_3_2;
in
  openssl.overrideAttrs (old: {
    name = "${openssl.name}+${oqs-provider.name}";
    postInstall =
      (old.postInstall or "")
      + ''
        install -Dpm555 ${oqs-provider}/lib/ossl-modules/oqsprovider.so $out/lib/ossl-modules/oqsprovider.so

        patch -i${./patches/oqsprov_conf.patch} $etc/etc/ssl/openssl.cnf
      '';
  })
