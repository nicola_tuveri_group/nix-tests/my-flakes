{
  lib,
  stdenv,
  fetchFromGitHub,
  cmake,
  ninja,
  pkg-config,
  openssl,
  enableStatic ? stdenv.hostPlatform.isStatic,
}:
stdenv.mkDerivation rec {
  pname = "liboqs";
  version = "0.12.0";

  src = fetchFromGitHub {
    owner = "open-quantum-safe";
    repo = pname;
    rev = version;
    sha256 = "sha256-ngjN1JdmnvMn+UXJeCiBwF1Uf7kTOjHVBL99xzoZVFY=";
  };

  patches = [
    ./patches/fix_pkgconfig.patch
  ];

  nativeBuildInputs = [cmake ninja pkg-config];
  buildInputs = [openssl openssl.dev];

  cmakeFlags = [
    "-GNinja"
    "-DBUILD_SHARED_LIBS=${
      if enableStatic
      then "OFF"
      else "ON"
    }"
    "-DOQS_DIST_BUILD=ON"
    "-DOQS_BUILD_ONLY_LIB=ON"
  ];

  dontFixCmake = true; # fix CMake file will give an error

  meta = with lib; {
    description = "C library for prototyping and experimenting with quantum-resistant cryptography";
    homepage = "https://openquantumsafe.org";
    license = licenses.mit;
    platforms = platforms.all;
    maintainers = [];
  };
}
