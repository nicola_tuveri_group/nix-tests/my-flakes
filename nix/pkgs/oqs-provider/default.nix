{
  lib,
  stdenv,
  fetchFromGitHub,
  cmake,
  pkg-config,
  ninja,
  openssl,
  liboqs,
  enableStatic ? stdenv.hostPlatform.isStatic,
}:
stdenv.mkDerivation rec {
  pname = "oqs-provider";
  version = "0.7.1-dev-dfa44a9";

  src = fetchFromGitHub {
    owner = "open-quantum-safe";
    repo = pname;
    #rev = version;
    rev = "dfa44a9775b99b3277744c675adb437e1edb8c04";
    sha256 = "sha256-sijfQuqtjObCiY2tEDuGae83o0uhqp3tRFbTo2egI6c=";
  };

  nativeBuildInputs = [cmake ninja pkg-config];
  buildInputs = [openssl openssl.dev liboqs];

  installPhase = ''
    mkdir -p $out
    install -Dpm444 lib/oqsprovider.so $out/lib/ossl-modules/oqsprovider.so
  '';

  dontFixCmake = true; # fix CMake file will give an error

  meta = with lib; {
    description = "OpenSSL 3 provider containing post-quantum algorithms";
    homepage = "https://openquantumsafe.org";
    license = licenses.mit;
    platforms = platforms.all;
    maintainers = [];
  };
}
