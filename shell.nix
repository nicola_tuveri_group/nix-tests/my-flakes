{
  pkgs ? import <nixpkgs> {},
  localPackages ? {},
  allLocalPackages ? [],
}: let
  nixConf = import ./nix/conf.nix;
  options = [
    ''--option extra-trusted-substituters "${builtins.concatStringsSep " " nixConf.binaryCaches}"''
    ''--option extra-trusted-public-keys "${builtins.concatStringsSep " " nixConf.binaryCachePublicKeys}"''
    ''--option experimental-features "nix-command flakes"''
  ];
in
  pkgs.mkShell {
    name = "nyx";
    nativeBuildInputs = with pkgs; [
      git
      openssl_3_2
    ];
    buildInputs =
      (with pkgs; [
        ])
      ++ allLocalPackages;
  }
