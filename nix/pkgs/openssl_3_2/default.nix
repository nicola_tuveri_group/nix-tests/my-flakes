{system}: let
  pinnedPkgs = builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/47c1824c261a343a6acca36d168a0a86f0e66292.tar.gz";
    sha256 = "sha256:0wwsc4ywn9xp9y2pkbxq3kkmhm5gliwmh308bq4gvc7w1mds19mn";
  };

  customPkgs = import pinnedPkgs {
    inherit system;
    overlays = [
      (self: super: {
        openssl_3_2-debug = super.enableDebugging (super.openssl_3_2.overrideAttrs (oldAttrs: {
          configureFlags = (oldAttrs.configureFlags or []) ++ ["--debug"];
        }));
      })
    ];
  };
in
  customPkgs.openssl_3_2-debug
