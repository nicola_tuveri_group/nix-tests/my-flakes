{
  description = "One Flake to rule them all, One Flake to find them, One Flake to bring them all and in the darkness bind them";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";

    flake-compat.url = "github:edolstra/flake-compat";
    flake-compat.flake = false;
  };

  outputs = {self, ...} @ inputs:
    with self.lib; let
      systems = ["x86_64-linux" "aarch64-darwin"];
      foreachSystem = genAttrs systems;
      pkgsBySystem = foreachSystem (
        system:
          import inputs.nixpkgs {
            inherit system;
            config = import ./nix/config.nix;
            overlays = attrValues self.overlays."${system}";
          }
      );
    in rec {
      lib = import ./lib {inherit inputs;} // inputs.nixpkgs.lib;

      formatter = foreachSystem (system: pkgsBySystem."${system}".alejandra);
      legacyPackages = pkgsBySystem;
      packages = foreachSystem (system: import ./nix/pkgs self system);
      defaultPackage = foreachSystem (system: self.packages."${system}".openssl_3_2);
      overlay = foreachSystem (system: _final: _prev: self.packages."${system}");
      devShell = foreachSystem (system:
        import ./shell.nix rec {
          pkgs = pkgsBySystem."${system}";
          localPackages = packages."${system}";
          allLocalPackages = attrValues localPackages;
        });

      overlays = foreachSystem (
        system:
          with inputs; let
            ovs = import ./nix/overlays {
              self = self;
              inherit system;
            };
          in
            {localPackages = self.overlay."${system}";} // ovs
      );
    };
}
